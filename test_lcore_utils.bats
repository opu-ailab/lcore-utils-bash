#!/usr/bin/env bats

setup() {
    return
}

teardown() {
    unset LCORE_LOG_LEVEL
    return 
}

@test "execute a command with the ordinary style when unset env var" {
    . ./lcore_utils.bash
    result="$(lcore_system '/bin/echo 1')"
    [ "$result" = "1" ]
}

@test "return status code properly from lcore_system" {
    run ./run_lcore_utils_test.bash
    [ "$status" -eq 0 ]
}

@test "output an executed command when set env var" {
    export LCORE_LOG_LEVEL=info
    run ./run_lcore_utils_test.bash
    [ "$status" -eq 0 ]
    [ "${lines[0]}" = "EXEC: /bin/echo 1" ]
    [ "${lines[1]}" = "1" ]
}

@test "lcore_read_text affects nothing when unset env var" {
    . ./lcore_utils.bash
    result="$(lcore_read_text test.txt)"
    [ "$result" = "" ]
}

@test "lcore_read_text displays filename when set env var" {
    . ./lcore_utils.bash
    export LCORE_LOG_LEVEL=info
    result="$(lcore_read_text test.txt)"
    [ "$result" = "READ: test.txt" ]
}

@test "lcore_write_text affects nothing when unset env var" {
    . ./lcore_utils.bash
    result="$(lcore_write_text test.txt)"
    [ "$result" = "" ]
}

@test "lcore_write_text displays filename when set env var" {
    . ./lcore_utils.bash
    export LCORE_LOG_LEVEL=info
    result="$(lcore_write_text test.txt)"
    [ "$result" = "WRITE: test.txt" ]
}
