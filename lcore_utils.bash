#!/bin/bash

lcore_system() {
    if [ "$LCORE_LOG_LEVEL" = "info" ]; then
        echo "EXEC: $1"
    fi
    eval $1
    return $?
}

lcore_read_text() {
    if [ "$LCORE_LOG_LEVEL" = "info" ]; then
        echo "READ: $1"
    fi
    return $?
}

lcore_write_text() {
    if [ "$LCORE_LOG_LEVEL" = "info" ]; then
        echo "WRITE: $1"
    fi
    return $?
}
